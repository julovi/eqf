function y = SPK_crossFade(azSpa, elSpa, elevation)
% SPK_CROSSFADE crossfade azimuthal and elevation spatialization 
%  
% SYNOPSIS: y = SPK_crossFade(azSpa, elSpa, elevation)  
% 
% INPUT azSpa: the result of azimuthal spatialization  
%       elSpa: the restul of elevational spatialization
%       elevation: the elevation angle in degrees
% 
% OUTPUT y: the two spatializations linearly interpolated.  
% 
% REMARKS This is just an example, it won't run 
% 
% SEE ALSO testHelpFunction 
% 
% AUTHOR    : Julian Villegas 
% $DATE     : 03-Apr-2017 14:07:27 $ 
% $Revision : 1.00 $ 
% DEVELOPED : 9.2.0.538062 (R2017a) 
% FILENAME  : SPK_crossFade.m 
e = deg2rad(elevation);
gainAzi = abs(cos(e));
gainEle = abs(sin(e));

y = azSpa.*gainAzi + elSpa*gainEle;

end 
% ===== EOF ====== [SPK_crossFade.m] ======  
