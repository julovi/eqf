function idx = SPK_findSpeakers(locations,a)
% SPK_FINDSPEAKERS returns indices of speaker locations to be used in 
% the spatialization. Returns a single index if there's a loudspeaker on the 
% desired location, or the two closest loudspeakers around the desired
% location.
%
% SYNOPSIS: idx = SPK_findSpeakers(locations,a)
%
% INPUT locations: locations of the loudspeakers
%       a: azimuth of the sound source
%
% OUTPUT idx: array with the location indices of the nearest loudspeakers
%
% REMARKS 
%
% SEE ALSO 
%
% AUTHOR    : Julian Villegas
% $DATE     : 28-Mar-2017 08:49:12 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : SPK_findSpeakers.m
if ismember(a,locations(1,:))
    idx = find(locations(1,:)==a, 1, 'last');
else
    idx = [find(locations(1,:)<a, 1, 'last'); ...
           find(locations(1,:)>a, 1)];
end
end
% ===== EOF ====== [SPK_findSpeakers.m] ======
