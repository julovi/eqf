function hmin = minPhaseHRIR(h)
% MINPHASEHRIR converts an HRIR into its minimum-phase filter version 
%  
% SYNOPSIS: hmin = minPhaseHRIR(h)  
% 
% INPUT hrirs: a mxnx2  matrix of n hrirs with m taps 
% 
% OUTPUT hmin: the mxnx2 matrix in minimum-phase representation. 
% 
% REMARKS This method was proposed by M. O. Hawksford. 
% “Digital signal processing tools for loudspeaker evaluation and 
% discrete-time crossover design”. J. Audio Eng. Soc., 45(1/2), 37–62 (1997).
% 
% SEE ALSO 
% 
% AUTHOR    : Julian Villegas 
% $DATE     : 24-Mar-2017 10:49:20 $ 
% $Revision : 1.00 $ 
% DEVELOPED : 9.1.0.441655 (R2016b) 
% FILENAME  : minPhaseHRIR.m 
    hmin =  real(ifft(exp(conj(hilbert(log(abs(fft(h))))))));
end 
% ===== EOF ====== [minPhaseHRIR.m] ======  
