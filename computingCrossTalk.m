function computingCrossTalk()
% COMPUTINGCROSSTALK simulates what happens in loudspeaker reproduction of
% binaural signals.
% 
% SYNOPSIS: a=testhelp(b,c)
% 
% INPUT b: some input parameter
% c: (opt) some optional input parameter. Default: []
% 
% OUTPUT a: some output parameter
% 
% REMARKS This is just an example, it won't run
% 
% SEE ALSO testHelpFunction
% 
% AUTHOR    : Julian Villegas
% $DATE     : 18-May-2017 09:26:36 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : computingCrossTalk.m
db = 'PKU&IOA'; %Qu's database
sr = 2^16; % Qu's database sampling rate
N=1024;
halfN = N/2;
%Define loudspeakers
spAzi = [0,330,120,240,30];
%distance from the center of the head to the loudspeakers in the lab
spDis = 160;
spEle = 0;
m = SPK_defineSpeakers(spAzi,spEle,spDis);
spHs = extractHRIRs(m(1:end-1,:)',db); % retrieve speaker impulse responses
azAz = [0];
elEl = -40:10:90;
diDi = 160;
invertThrs = 300;


for i=1:length(azAz)
    for j = 1:length(elEl)
        idx = [2,5];
        theAzi = azAz(i);
        source_loc = [theAzi,elEl(j),diDi];
        soHs = extractHRIRs(source_loc);
%        oriRMS = reshape(rms(soHs),[1,2]);
        G = conv(minPhaseHRIR(spHs(:,idx(1),1)),minPhaseHRIR(spHs(:,idx(2),2))) - ...
            conv(minPhaseHRIR(spHs(:,idx(1),2)),minPhaseHRIR(spHs(:,idx(2),1)));
        G = SPK_invert(G,invertThrs);
        G = minPhaseHRIR(G);% 1/H
        xl = conv(minPhaseHRIR(soHs(:,1)),minPhaseHRIR(spHs(:,idx(2),2)))-...
            conv(minPhaseHRIR(soHs(:,2)),minPhaseHRIR(spHs(:,idx(1),2)));
        xl = conv(xl,G);
        xr = conv(minPhaseHRIR(soHs(:,2)),minPhaseHRIR(spHs(:,idx(1),1)))-...
            conv(minPhaseHRIR(soHs(:,1)),minPhaseHRIR(spHs(:,idx(2),1)));
        xr = conv(xr,G);
        
        yl = conv(xl,minPhaseHRIR(spHs(:,idx(1),1))) + ...
            conv(xr,minPhaseHRIR(spHs(:,idx(2),1)));
        yl = yl(1:1024);
        yr = conv(xl,minPhaseHRIR(spHs(:,idx(1),2))) + ...
            conv(xr,minPhaseHRIR(spHs(:,idx(2),2)));
        yr = yr(1:1024);
        y = [yl, yr];
        subplot(1,2,1);
        plotmaglogf(soHs(:,1), sr, N, 'b', '-') % desired HRTF
        hold on
        plotmaglogf(spHs(:,idx(1),1), sr, N, 'g', ':') % left signal, speaker 1
        plotmaglogf(spHs(:,idx(2),1), sr, N, 'm', ':') % left signal, speaker 2
        plotmaglogf(y(:,1), sr, N, 'k', '-') % left signal at ear
        legend('target', 'spk1', 'spk2', 'atEar','Location','northwest')
        hold off
        
        subplot(1,2,2);
        plotmaglogf(soHs(:,2), sr, N, 'r', '-')
        hold on
        plotmaglogf(spHs(:,idx(1),2), sr, N, 'g', ':') % right signal, speaker 1
        plotmaglogf(spHs(:,idx(2),2), sr, N, 'm', ':') % right signal, speaker 2
        plotmaglogf(y(:,2), sr, N, 'k', '-') % right signal at ear
        legend('target', 'spk1', 'spk2', 'atEar','Location','northwest')
        hold off
        
        name = sprintf('CTCA%dE%dD%d',azAz(i),elEl(j),diDi);
        
        set(gcf,'NextPlot','add');
        axes;
        h = title(name);%
        set(gca,'Visible','off');
        set(h,'Visible','on');
        set(h,'Position',get(h,'Position')+[0 .03 0]);  % move up slightly
        fig=gcf;
        set(findall(fig,'-property','FontSize'),'FontSize',14)
        set(fig,'PaperPositionMode','auto');
        set(fig,'PaperOrientation','landscape');
        set(fig,'Position',[20 20 800 400]);
        print(['./plots/' name '.pdf'],'-dpdf')    
    end
end

end
%===== EOF ====== [computingCrossTalk.m] ======
