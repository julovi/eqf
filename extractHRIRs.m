function h = extractHRIRs(aed, path)
% EXTRACTHRIRS extract the HRIRs speficied by the rows of aed array and
% returns them in the mxnx2 h matrix, where m is the number of taps, n the
% number of hrirs, and left and right channels are stored in the third
% dimension.
%
% SYNOPSIS: h = extractHRIRs(aed,path)
%
% INPUT aed: an array of locations with amplitude, elevation, and distance
%            for columns. Amplitude and elevation are given in degrees.
%       path: path to database 'PKU&IOA'
%
% OUTPUT h: a matrix of hrirs
%
% REMARKS
%
% SEE ALSO
%
% AUTHOR    : Julian Villegas
% $DATE     : 23-Mar-2017 14:20:14 $
% $Revision : 1.00 $
% DEVELOPED : 9.1.0.441655 (R2016b)
% FILENAME  : extractHRIRs.m
if nargin==0
    error('not enough arguments');
end
m = 1024;% PKU&IOA has 1024 taps per HRIR
n = size(aed,1);
h = zeros(m,n,2);
for i=1:n
    h(:,i,:) = readEqualizedHrir(path,aed(i,3),aed(i,2),aed(i,1),'r');
end
end
% ===== EOF ====== [extractHRIRs.m] ======
