function eqf_spatialization()
% path for the raw source
rawSource = './raw/Drums.wav';
% path for the processed audio
dest = './spatializedItems/';
%path to Qu's database
path = './database/';
% Qu's database sampling rate
sr = 2^16;
% Define loudspeaker azimuths
spAzi = [0,30,120,240,330];%
% distance from the center of the head to the loudspeakers
spDis = 160;
% elevation of the loudspeakers
spEle = 0;
% associate loudspeakers to channels for saving
m = SPK_defineSpeakers(spAzi,spEle,spDis,'wavex');
% retrieve speaker impulse responses
spHs = inferHRIR(m(1:end-1,:)',path);

% define desired location(s)
azAz = [0 45 90 135 180 270 315];
elEl = [0 30 60 90];
diDi = 159;
longaudio = zeros(length(sr),6);

% load the audio
[a,sro] = audioread(rawSource);
if size(a,2) > 1
    warning('Stereo signal detected, mixing both channels');
    a = sum(a,2)/2;
end
if(sr ~= sro)
    a = resample(a,sr,sro);
end
% center and remove DC
a = a - mean(a);
% Normalize amplitude to be 0dB FS
a = a/max(abs(a));

for i=1:length(azAz)
    for j = 1:length(elEl)
        % Define the source location
        source_loc = [azAz(i),elEl(j),diDi];
        soHs = inferHRIR(source_loc, path);
        
        idx = SPK_findSpeakers(m,source_loc(1));
        % find the azimuthal spatialization
        azSpa = SPK_azimuthSpat(a,soHs,spHs,m,idx);
        
        if elEl(j) ~= 0
            % find the elevational spatialization
            elSpa = SPK_elevationSpat(a,soHs,spHs,m);
            % cross-fade azimuthal and elevational spatializations, preserving RMS
            y = SPK_crossFade(azSpa, elSpa, source_loc(2));
        else
            y = azSpa;
        end
        
        % save
        if(sr ~= sro)
            y = resample(y,sro,sr);
        end
        % 0 and 360 are the same, one is zeros, the other is not.
        len =  size(m,2);
        y(:,1) = y(:,1)+ y(:,len);
        audio = zeros(length(y),len);
        for k=1:len-1
            audio(:,m(4,k)) = y(:,k);
        end
        audiowrite([dest num2str(elEl(j)) '_' ...
            num2str(azAz(i)) ... %'_' ...
            '.wav'], ...
            audio, sro, 'BitsPerSample', 24);
        longaudio = [longaudio; audio];
    end
end
audiowrite([dest 'longaudio.wav'], ...
    longaudio, sro, 'BitsPerSample', 24);
disp 'done.'

end
% ===== EOF ====== [createExperimentPractice.m] ======
