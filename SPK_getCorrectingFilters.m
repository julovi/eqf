function corHs = SPK_getCorrectingFilters(spHs,idx,soHs)
% SPK_GETCORRECTINGFILTERS returns correcting filters corresponding to the
% convolution of the desired hrir with inverse of the average hrir for the
% locations of loudspeakers.
%
% SYNOPSIS: corHs = SPK_getCorrectingFilters(spHs,idx)
%
% INPUT spHs: mxnx2 matrix with loudspeaker impulse responses, where m is
% the number of taps, n the number of hrirs, and left and right channels
% are stored in the third dimension.
%       idx: column vector with the speakers to use
%       soHs: the hrir for the desired location
%
% OUTPUT corHs: a 2xm matrix with the correcting filters for left and right
% channels.
%
% REMARKS
%
% SEE ALSO
%
% AUTHOR    : Julian Villegas
% $DATE     : 28-Mar-2017 11:50:23 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : SPK_getCorrectingFilters.m

if length(idx)> 1
    m=size(spHs,1);
    halfm = m/2;
    
    % Energy spectrum density in each channel (energy/Hz)
    hs = abs(fft(spHs(:,idx,:))).^2;
    
    % compute average of the densities
    meanHs = sqrt(sum(hs,3)./length(idx));
    meanHs = rotate_vect(real(ifft(meanHs)),halfm);
    meanHs = SPK_invert(meanHs,25);
    meanHs = minPhaseHRIR(meanHs);% 1/Hs
    corHs = zeros(size(meanHs,1)+size(soHs,1)-1,size(meanHs,2));
    for i=1:size(meanHs,2)
        corHs(:,i) = conv(meanHs(:,i),soHs(:,i));
        corHs(:,i) = minPhaseHRIR(corHs(:,i)); % convert this to minPhase filter
    end
else
    corHs = zeros(size(spHs,1),2);
    corHs(1,:) = 1;
end
% ===== EOF ====== [SPK_getCorrectingFilters.m] ======
