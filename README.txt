These Matlab routines are used to create spatializations via inverse filters and loudspeaker grouping in the way is explained in [1].

To use them, please open the file "eqf_spatialization.m" and edit it according to your needs (destination path, audio to spatialize, etc.). These routines are meant to be used with a diffuse-field equalized version of the database provided by Qu et al. [2]. That database can be found here:

http://www.cis.pku.edu.cn/auditory/Staff/Dr.Qu.files/Qu-HRTF-Database.html

Note that this database is copyrighted by the Key Lab of Machine Perception (Ministry of Education), Peking University. It is provided free with no restrictions on research use, provided the authors are cited when the data is used. And it is forbidden to be used in any commercial application.
 
The code was developed with Matlab Version 9.1.0.441655 (R2016b), it works with current versions of Matlab (9.5.0.882065 (R2018b) Prerelease) and probably with some earlier versions as well. It uses the Signal Processing Toolbox.


References:
[1] J. Villegas, “Elevation perception improvement in loudspeakers rings by means of equalizing filters,” Acoust. Sci. & Tech., 2018. 
[2] T. Qu, Z. Xiao, M. Gong, Y. Huang, X. Li, and X. Wu, “Distance-Dependent Head-Related Transfer Functions Measured With High Spatial Resolution Using a Spark Gap,” IEEE Trans. on Audio, Speech & Language Processing, vol. 17, no. 6, pp. 1124–1132, 2009.