function m = SPK_defineSpeakers(spAzi,spEle,spDis,format)
% SPK_defineSpeakers associate the speaker locations to audio channels.
% If format is wavex, channels are set as in a regular 5.1 channel system
% defined by
% https://msdn.microsoft.com/en-us/library/windows/hardware/dn653308(v=vs.85).aspx
% That is:
% ch1: Front Left - FL
% ch2: Front Right - FR
% ch3: Front Center - FC
% ch4: Low Frequency - LF % Not used
% ch5: Back Left - BL
% ch6: Back Right - BR
%
% SYNOPSIS: m = SPK_defineSpeakers(spAzi,spEle,spDis, format)
%
% INPUT spAzi: the azimuths in degrees of the main speakers
% starting from the front center, and moving clock-wise.
%       spEle: a scalar indicating the elevation of the loudspeakers
%       spDis: a scalar indicating the radius of the speaker array
%       format: 'wavex' for 5.1 surround wav-extended format
%       
%
% OUTPUT m: a matrix with azimuths, elevations, distances, and channels as
% rows.
%
% REMARKS The front channel is repeated for easing computations
%
% SEE ALSO
%
% AUTHOR    : Julian Villegas
% $DATE     : 28-Mar-2017 08:29:46 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : SPK_defineSpeakers.m
if nargin < 4
    format = 'free';
end
spAzi = [spAzi, 360];% repeat 0 as 360 for convenience.
len = length(spAzi);
spEle = repmat(spEle,len,1)';
spDis = repmat(spDis,len,1)';
if strcmpi(format,'wavex')
    chann = [3,2,6,5,1,3];
else
    chann = 1:len;
end
m = [spAzi; spEle; spDis; chann];
end
% ===== EOF ====== [SPK_defineSpeakers.m] ======
