function y=computeCrossTalk(idx,X,H)
% COMPUTECROSSTALK computes the crosstalk in speaker reproduction systems
%
% SYNOPSIS: a=computeCrossTalk(idx,X,H)
%
% INPUT idx: which loudspeakers to use
%       X: intended binaural signal
%       H: array of HRTFs corresponding to each loudspeaker
%
% OUTPUT y: The resulting HRTF
%
% REMARKS The nomenclature here follows that of "Transaural 3-D audio" by
% William Gardner, specially Figure 3
%
% SEE ALSO comparingMethodsAzi
%
% AUTHOR    : Julian Villegas
% $DATE     : 05-Dec-2017 09:54:42 $
% $Revision : 1.00 $
% DEVELOPED : 9.3.0.713579 (R2017b)
% FILENAME  : computeCrossTalk.m
if length(idx) == 1
    yl = H(:,idx(1),1);
    yr = H(:,idx(1),2);
else
    yl = conv(X(:,1),H(:,idx(1),1)) + conv(X(:,2),H(:,idx(2),1));
    yr = conv(X(:,1),H(:,idx(1),2)) + conv(X(:,2),H(:,idx(2),2));
end
y = [yl, yr];
end
% ===== EOF ====== [computeCrossTalk.m] ======
