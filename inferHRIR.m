function h = inferHRIR(p,path)
% INFERHRIR interpolates existing HRIRs to find a HRIR of a given location
% that may not be in the database.
%
% SYNOPSIS: h = inferHRIR(p,path)
%
% INPUT p: an array of locations in spherical coordinates (a,e,d) in degrees
% and cm. One location per row.
%       path: path to the database.
%
% OUTPUT h: the resulting interpolation, i.e., hrir in as minphase filters
% with the appropriate delay applied to them.
%
% REMARKS It's better not to use this function for actual speaker
% locations. Instead, choose a location available in the database and use
% extractHRIR instead. Currently, only PKU&IOA database is supported
%
% SEE ALSO extractHRIR
%
% AUTHOR    : Julian Villegas
% $DATE     : 28-Mar-2017 18:46:21 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : inferHRIR.m
if nargin==0
    error('not enough arguments');
end
n = size(p,1);
h = zeros(1024,n,2);
load('Delaunay_PKU&IOA_HRTF.mat','dt');
sr = 2^16;
p = sphToRect(p);
for i=1:n
    source_pos = p(i,:);
    idx = pointLocation(dt,source_pos);
    while isnan(idx)
        % dunno why Matlab fails, I contacted them and found no solution, 
        % to circumvent that, here I add some random noise to the location
        % until it works.
        tmpP = [source_pos(1)+rand()-0.5,...
            source_pos(2)+rand()-0.5,...
            source_pos(3)+rand()-0.5];
        idx = pointLocation(dt,tmpP);
        disp('**')
    end
    c = dt.ConnectivityList(idx,:);
    c = dt.Points(c,:);
    ws = cartesianToBarycentric(dt,idx,source_pos);
    [a,e,d] = cart2sph(c(:,1),c(:,2),c(:,3));
    [a,e,d] = rad_to_deg(a,e,d);
    aed = [a,e,d];
    if sum(ws>1)
        ws = round(ws);
    end
    if sum(ws>1) || isnan(sum(ws))
        ws = ones(1,length(ws))/length(ws);
        disp('oo');
    end
    hrirs = extractHRIRs(aed,path);
    [dlr,dl,dr] = computeITD(hrirs,sr);
    itds = [dlr,dl,dr];
    mPhase = minPhaseHRIR(hrirs);
    
    htemp = interpolateMinPhase(mPhase,ws);
    t = interpolateITDs(itds,ws);
    
    % apply ITD
    htemp = applyDelay(htemp,t,sr);
    htemp = htemp(1:1024,:);
    h(:,i,:) = reshape(htemp,[1024,1,2]);
end
% ===== EOF ====== [inferHRIR.m] ======
