function y = applyDelay(h,t,sr)
% APPLYDELAY applies delays to left and right channel of an HRIR as
% specified by the vector t. 
%
% SYNOPSIS: y = applyDelay(t,sr)
%
% INPUT h: an hrir in minium phase version
%       t: an array with ITD, and delays of the left and right channel.
%       sr: sampling rate
%
% OUTPUT y: the delayed version of h
%
% REMARKS 
%
% SEE ALSO 
%
% AUTHOR    : Julian Villegas
% $DATE     : 29-Mar-2017 10:48:10 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : applyDelay.m

t = round(t*sr);
delL =  zeros(t(2),1);
delR =  zeros(t(3),1);
y = [[delL; h(:,1);delR], [delR;h(:,2); delL]];
end
% ===== EOF ====== [applyDelay.m] ======
