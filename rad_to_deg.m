function [a,e,d] = rad_to_deg(a,e,d)
% RAD_TO_DEG returns a point in spherical coordinates with 0<a<360 degrees
%  
% SYNOPSIS: [a,e,d] = rad_to_deg(a,e,d)  
% 
% INPUT a:azimuth  
%       e: elevation
%       d: distance
% 
% OUTPUT the same values corrected as in the description 
% 
% REMARKS  
% 
% SEE ALSO  
% 
% AUTHOR    : Julian Villegas 
% $DATE     : 25-Mar-2017 10:28:22 $ 
% $Revision : 1.00 $ 
% DEVELOPED : 9.1.0.441655 (R2016b) 
% FILENAME  : rad_to_deg.m 
a = rad2deg(a);
idx = find(a<0);
while ~isempty(idx)
    a(idx) = a(idx)+360;
    idx = find(a<0);
end
idx = find(a>360, 1);
if ~isempty(idx)
    a(idx) = mod(a(idx),360);
end
e = rad2deg(e);
end 
% ===== EOF ====== [rad_to_deg.m] ======  
