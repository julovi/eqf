function y = SPK_azimuthSpat(a,soHs,spHs,m,idx)
% SPK_AZIMUTHSPAT spatialize a sound using the inverse filter method, no
% ITD is applied.
%
% SYNOPSIS: aziSpa = SPK_azimuthSpat(a,sro,source_pos,m,db,sr)
%
% INPUT a: audio samples
%       soHs: HRIR of the desired location
%       spHs: HRIRs of the loudspeakers
%       m : matrix with speaker locations in columns (rows correspond to 
%           azimuth, elevation, distance, and channel) 
%       idx: a column vector indicating which speaker HRIRs to use
%
% OUTPUT y: the spatialized sound
%
% REMARKS the filters serve as an equalizer to compensate for the average
% energy loss/gain given by the location of the loudspeakers. The ITD
% should be applied later, taking into consideration the inherent delay of
% each speaker and that of the virtual sound source.
%
% SEE ALSO
%
% AUTHOR    : Julian Villegas
% $DATE     : 28-Mar-2017 09:57:53 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : SPK_azimuthSpat.m
oriRMS = reshape(rms(soHs),[1,2]);
hLen = length(spHs);
corHs = SPK_getCorrectingFilters(spHs,idx, reshape(soHs,[hLen,2]));
corHs = corHs(1:hLen,:); % 1024 is sufficient
corHs = corHs.*oriRMS./rms(corHs); % RMS normalization

y = zeros(length(a)+1024-1,size(m,2));
% convolve with sound
if length(idx)<2
    y(:,idx) = (conv(a,corHs(:,1))+ conv(a,corHs(:,2))).*(2^.5/2); % 
else
    y(:,idx) = [conv(a,corHs(:,1)), conv(a,corHs(:,2))];
end

if sum(m(1,idx) > 90)==2 && sum(m(1,idx) < 270)== 2
    % we're at the back of the listener
    y(:,idx)=y(:,wrev(idx));
end
end
% ===== EOF ====== [SPK_azimuthSpat.m] ======
