function y = SPK_invert(x,lim,n)
% SPK_INVERT creates inverse filter of x
%
% SYNOPSIS: y = SPK_invert(x,lim,n)
%
% INPUT x: minimum phase filter
%       lim: maximum range of inverse filter in dB (default 30).
%       n: size of inverse filter (default length(x))
%
% OUTPUT y: the inverse filter
%
% REMARKS This is an adaptation of Gardner's original function made by
% Julian Villegas.
%
% SEE ALSO 
%
% AUTHOR    : Bill Gardner. Copyright 1995 MIT Media Lab. All rights reserved
% $DATE     : 28-Mar-2017 19:38:02 $
% $Revision : 1.00 $
% DEVELOPED : 9.2.0.538062 (R2017a)
% FILENAME  : SPK_invert.m
if (nargin < 3)
    n = length(x);
end
if (nargin < 2)
    lim = 20;
end

X = fft(x,n);
Xlogmag = 20*log10(abs(X));

% Find average log magnitude over 6 octaves, ignoring one octave
% each at top and bottom of usual 8 octave range.
lo = ceil(n / (2^8));
hi = floor(n / (2^2));
Xavglogmag = zeros(size(x,2),1);
for i=1:size(x,2)
    Xavglogmag(i) = sum(Xlogmag(lo:hi,1));
end
Xavglogmag = Xavglogmag/(hi-lo+1);

% Subtract average, clip, and restore average.
halflim = lim / 2;
for i=1:size(x,2)
    for k = 1 : n
        Xlogmag(k,i) = Xlogmag(k,i) - Xavglogmag(i);
        if (Xlogmag(k,i) > halflim)
            Xlogmag(k,i) = halflim;
        elseif (Xlogmag(k,i) < -halflim)
            Xlogmag(k,i) = -halflim;
        end
        % this line gives inverse filter that varies around 0 dB.
        Xlogmag(k,i) = Xlogmag(k,i) + Xavglogmag(i);
    end
end

% Y is inverse spectrum (zero phase).
Y = 10.^(-Xlogmag/20) .* exp(-1i * angle(X));

% Take ifft and rotate so that maximum value is centered
y = real(ifft(Y));
for i=1:size(x,2)
    [~,ymaxi] = max(y(:,i));
    y(:,i) = rotate_vect(y(:,i), floor(n/2) - ymaxi);
end
end
% ===== EOF ====== [SPK_invert.m] ======
