function y = SPK_elevationSpat(a,soHs,spHs,m)
% SPK_ELEVATIONSPAT spatializes a sound to be reproduced in a speaker array. 
% Using only the left and right loudspeaker as groups.
%  
% SYNOPSIS: y = SPK_elevationSpat(a,soHs,spHs)  
% 
% INPUT  a: audio samples
%       soHs: HRIR of the desired location
%       spHs: HRIRs of the loudspeakers
%       m: the matrix with speaker information
%
% OUTPUT y: the spatialized sound 
% 
% REMARKS The front loudspeaker is not used. 
% 
% SEE ALSO SPK_azimuthSpat 
% 
% AUTHOR    : Julian Villegas 
% $DATE     : 03-Apr-2017 13:38:03 $ 
% $Revision : 1.00 $ 
% DEVELOPED : 9.2.0.538062 (R2017a) 
% FILENAME  : SPK_elevationSpat.m 

y = zeros(length(a)+1024-1,size(m,2));
oriRMS = reshape(rms(soHs),[1,2]);
hLen = length(spHs);
idx = find(m(1,:) > 180 & m(1,:) < 360);
leftHs = SPK_getCorrectingFilters(spHs,idx, reshape(soHs,[hLen,2]));
leftHs = leftHs(1:hLen,:); % 1024 is sufficient
leftHs = leftHs.*oriRMS./rms(leftHs)/(2^.5/2); % RMS normalization
y(:,idx) = [conv(a,leftHs(:,1)), conv(a,leftHs(:,1))]; % convolve with sound

idx = find(m(1,:) > 0 & m(1,:) < 180);
rightHs = SPK_getCorrectingFilters(spHs,idx, reshape(soHs,[hLen,2]));
rightHs = rightHs(1:hLen,:); % 1024 is sufficient
rightHs = rightHs.*oriRMS./rms(rightHs)/(2^.5/2); % RMS normalization
y(:,idx) = [conv(a,rightHs(:,2)), conv(a,rightHs(:,2))]; % convolve with sound
end 
% ===== EOF ====== [SPK_elevationSpat.m] ======  
